package com.company;

import com.company.model.Producer;
import com.company.model.Song;
import com.company.utils.FileHandler;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {

        //Lists can not be created with the following syntax
        // as they are abstract classes

        //List number = new List();

        //But we can create an instance of a List
        // of a concrete class
        List<Song> songDb = new ArrayList<>();
        try{
            songDb = FileHandler.readFileSongs("src/com/company/resources/Songs.csv");
        }catch (IOException e){
            System.out.println("Error reading file");

        }catch (ClassCastException e){
            System.out.println("Error casting file");
        }


        Map<Long, Producer> producersDb = new HashMap<Long, Producer>();

        try{
            producersDb = FileHandler.readFileProducers("src/com/company/resources/Producers.csv");

        }catch (IOException e){
            System.out.println("Error reading file");

        }catch (ClassCastException e){
            System.out.println("Error casting file");
        }


        try {
            FileHandler.writeSongsToFile("DB_Songs.dat",songDb);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Reading the songs from the file
        try {
            List<Song> songsFromFile = FileHandler.readSongsFromFile("DB_Songs.dat");
            System.out.println(songsFromFile);
        }
        catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }

        //Writing list directly to a file
        try {
            FileHandler.writeObjectToFile("DB_Songs.dat",songDb);

        } catch (IOException e) {
            e.printStackTrace();
        }

        //Reading list directly from a file
        try {
            List<Song> songsFromFile =(ArrayList<Song>) FileHandler.readObjectFromFile("DB_Songs.dat");
        }  catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();

        }

        addNewSongToDb(songDb, producersDb);

        //Printing the songs
        for (Song song : songDb) {
            System.out.println(song);
        }








    }

    public static void addNewSongToDb(List<Song> songDb, Map<Long,Producer> producersDb){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter song name");
        String songName = scanner.nextLine();
        System.out.println("Enter song Artist");
        String songArtist = scanner.nextLine();
        System.out.println("Enter song genre");
        String songGenre = scanner.nextLine();
        System.out.println("Enter song duration");
        Integer songDuration = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter song year");
        Integer songYear = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter song Album");
        String songAlbum = scanner.nextLine();



        Song song = new Song(songName, songArtist, songGenre, songDuration, songYear, songAlbum);


        String answer;
          do {
               System.out.println("Please type Producer ID");
               Long producerId = Long.parseLong(scanner.nextLine());
               if (producersDb.containsKey(producerId)) {
                   song.addProducer(producersDb.get(producerId));
                   System.out.println("would you like to add another producer? (y/n)");
                   answer = scanner.nextLine();

               } else {
                   System.out.println("Producer not found");
                   answer = "y";
               }

          } while (answer.equalsIgnoreCase("y"));

            songDb.add(song);
        System.out.println("Song added to DB");


    }

    private static void linkSongToProducer(Song song, Map<Long, Producer> producersDb) {

        Set<Producer> auxProducers = song.getProducers();
        for (Producer producer : auxProducers) {
            producersDb.get(producer.getId()).addSong(song);
        }

    }


}
