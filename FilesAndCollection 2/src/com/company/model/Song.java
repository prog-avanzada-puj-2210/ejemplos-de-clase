package com.company.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Song implements Serializable {

    private Long id;
    private String name;
    private String artist;
    private String genre;
    private Integer duration;
    private String album;
    private Integer releaseYear;
    private Integer numberOfDownloads;
    private Set<Producer> producers;


    private static Long nextId = 1L;

    public Song(
                String name,
                String artist,
                String genre,
                Integer duration,
                Integer releaseYear,
                String album) {
        this.id = nextId++;
        this.name = name;
        this.artist = artist;
        this.genre = genre;
        this.duration = duration;
        this.releaseYear = releaseYear;
        this.album = album;
        this.numberOfDownloads = 0;
        this.producers = new HashSet<Producer>();
    }
    public Song(
            String name,
            String artist,
            String genre,
            Integer duration,
            Integer releaseYear,
            String album,
            Producer producer) {
        this.id = nextId++;
        this.name = name;
        this.artist = artist;
        this.genre = genre;
        this.duration = duration;
        this.releaseYear = releaseYear;
        this.album = album;
        this.numberOfDownloads = 0;
        this.producers = new HashSet<Producer>();
        this.producers.add(producer);
    }

    public void addProducer(Producer producer) {
        producers.add(producer);
        producer.addSong(this);

    }

    public Set<Producer> getProducers() {
        return producers;
    }

    public void removeProducerById(Long id) {
        for (Producer producer : producers) {
            if (producer.getId().equals(id)) {
                producers.remove(producer);
            }
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getDuration() {
        return duration;
    }

    public  Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Integer getNumberOfDownloads() {
        return numberOfDownloads;
    }

    public void setNumberOfDownloads(Integer numberOfDownloads) {
        this.numberOfDownloads = numberOfDownloads;
    }

    @Override
    public String toString() {
        return "Song{" + "id=" + id + ", name=" + name + ", artist=" + artist + ", genre=" + genre + ", duration=" + duration + ", album=" + album + ", numberOfDownloads=" + numberOfDownloads + '}'
                + "\nProducers: " + producers;
    }
}
