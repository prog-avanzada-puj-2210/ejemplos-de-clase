package com.company.model;

import java.util.ArrayList;

import java.util.List;
import java.util.Set;

public class Producer {

    private static Long nextId = 1L;
    private Long id;
    private String name;
    private String country;
    private String nickName;
    private List<Song> producedSongs;

    public Producer(
                     String name,
                     String country,
                     String nickName) {
        this.id = getNextId();
        this.name = name;
        this.country = country;
        this.nickName = nickName;
        this.producedSongs = new ArrayList<Song>();
    }

    private static synchronized Long getNextId() {
        return nextId++;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<Song> getProducedSongs() {
        return producedSongs;
    }


    public void addSong(Song song) {

        this.producedSongs.add(song);
    }

    public boolean removeSong(Long songId) {
        for (Song song : producedSongs) {
            if (song.getId().equals(songId)) {
                producedSongs.remove(song);
                return true;
            }
        }
        return false;
    }


    @Override
    public String toString() {
        return "Producer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", nickName='" + nickName + '\'' +
                ", producedSongs=" + producedSongs +
                '}';
    }
}


