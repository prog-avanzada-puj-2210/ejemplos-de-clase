package com.spotify;

import java.util.Scanner;

public class Main {
    private static Integer currentSong=0;
    private static Integer currentClient=0;
    public static void main(String[] args) {

        Song[] songs = new Song[100];
        Client[] clients = new Client[100];

        Scanner scanner = new Scanner(System.in);
        Integer input = 0;
        do{
            input = scanner.nextInt();
            switch (input){
                case 1:
                    if (addNewSongToDB(songs)){
                        System.out.println("Song added to DB");

                    }
                    else{
                        System.out.println("Error when adding song to DB");
                    };
                case 2:
                    break;
            }
        }while(input!= 0);
    }

    private static boolean addNewSongToDB(Song[] songs) {
        Scanner scanner = new Scanner(System.in);
        Song auxSong;
        //Long id;
        String name;
        String artist;
        String genre;
        Integer duration;
        String album;

        System.out.println("Please enter all the values of " +
                "the song you want to add:" );
        System.out.println("Type the name of your song: ");
        name = scanner.nextLine();
        System.out.println("Type the artist of your song: ");
        artist = scanner.nextLine();
        System.out.println("Type the genre of your song: ");
        genre =  scanner.nextLine();
        System.out.println("Type the duration of your song (in seconds): ");
        duration = scanner.nextInt();
        System.out.println("Type the name of the album of your song: ");
        album = scanner.nextLine();

        auxSong = new Song(0L,name, artist,genre,duration,album );
        songs[currentSong] = auxSong;

        return true;
    }


}
